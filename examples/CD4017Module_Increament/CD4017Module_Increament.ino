#include <CD4017Module.h>

CD4017Module cd4017Module = CD4017Module(13);

void setup() {
	cd4017Module.initial();
}

void loop() {
	delay(1000);
	cd4017Module.increament();
}