#ifndef CD4017MODULE_H
#define CD4017MODULE_H

#include <Arduino.h>

class CD4017Module {
	private:
		static const unsigned int DELAY_MICROSECONDS = 50;
		byte sckPin;
	public:
		CD4017Module(byte);
		void initial();
		void increament();
};

#endif