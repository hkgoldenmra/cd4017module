#include <CD4017Module.h>

CD4017Module::CD4017Module(byte sckPin) {
	this->sckPin = sckPin;
}

void CD4017Module::initial() {
	pinMode(this->sckPin, OUTPUT);
	digitalWrite(this->sckPin, LOW);
}

void CD4017Module::increament() {
	digitalWrite(this->sckPin, HIGH);
	delayMicroseconds(CD4017Module::DELAY_MICROSECONDS);
	digitalWrite(this->sckPin, LOW);
	delayMicroseconds(CD4017Module::DELAY_MICROSECONDS);
}